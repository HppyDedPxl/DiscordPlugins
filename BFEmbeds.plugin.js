//META{"name":"BFEmbeds"}*//

/* global PluginUtilities:false, ReactUtilities:false, BdApi:false */

// embedObjectDefinition :

class BFEmbeds {

	getName() { return "Better F@#$%ing Embeds!"; }
	getShortName() { return "BFE"; }
	getDescription() { return "Bigger and better embedding for PC. Because thumbnails are for sissies~!"; }
	getVersion() { return "1.0 - Alpha"; }
	getAuthor() { return "HppyDedPxl"; }





	constructor() {

		this.linkSelectors = ['.accessory'];
		// if any of the children accessories have this child class, the branch will NOT be deleted
		this.deleteExceptions = ['embedContent-AqkoYv', 'reactions','CEmbed_Embedding', 'attachment-1Vom9D']


	}

	applySettings(){
		this.MaxEmbedWidth = document.querySelector('input[name="MaxWidthInput"]').value;
		this.MaxEmbedHeight = document.querySelector('input[name="MaxHeightInput"]').value;
		bdPluginStorage.set('BFEmbeds','MaxWidth', this.MaxEmbedWidth);
		bdPluginStorage.set('BFEmbeds','MaxHeight',this.MaxEmbedHeight);
	}

	load() {

			this.MaxEmbedWidth = bdPluginStorage.get('BFEmbeds','MaxWidth');
			this.MaxEmbedHeight = bdPluginStorage.get('BFEmbeds','MaxHeight');
			if(this.MaxEmbedWidth === null)
			{
				this.MaxEmbedWidth = "100%";
			}
			if(this.MaxEmbedHeight === null)
			{
				this.MaxEmbedHeight = "720px";
			}


			this.SettingsHTML = '<h1>Better F@#$%ing Embeds!</h1>'+
									'<br />' +
									 '<label for="BFESettingsWidth">Maximum Embed Width (e.g. 100%, auto or 1024px) </label> ' +
									 '<br />' +
									 '<label><input name="MaxWidthInput" type="text" placeholder="100%" name="BFESettingsWidth" id="BFESettingsWidth" value="' + this.MaxEmbedWidth + '" /></label>' +
									 '<br />' +
									 '<label for="BFESettingsHeight">Maximum Embed Height (e.g. 100%, auto or 1024px) </label> ' +
									'<br />' +
									'<label><input name="MaxHeightInput" type="text" placeholder="100%" name="BFESettingsWidth" id="BFESettingsWidth" value="' + this.MaxEmbedHeight + '" /></label>' +
									'<br />' +
									 '<button onclick="BdApi.getPlugin(\'Better F@#$%ing Embeds!\').applySettings()"> <b>Apply</b> </button>';


		this.initialized = false;

		this.style = `
		.CEmbed_Embedding {
     -webkit-box-sizing: border-box;
     background: hsla(0,0%,98%,.3);
     border: 1px solid hsla(0,0%,80%,.3);
		 border-left: 5px solid hsla(0,0%,80%,.3);;
     border-radius: 0 3px 3px 0;
     box-sizing: border-box;
     overflow: hidden;
     padding: 8px 10px;
     background-color: rgba(46,48,54,.3);
     border-color: rgba(46,48,54,.6);
		 border-left-color: rgba(250,100,0,.8);
		 width: auto;
		 height: auto;
		}

		.CEmbed_Pill{
			background-color: #4f545c;
			border-radius: 3px 0 0 3px;
			flex-shrink: 0;
			max-width: auto;
			max-height: auto;
		}

		.CEmbed_YTFrame{
		width="600";
		height=auto;
		frameborder="0";
		}

		.CEmbed_Player{
			max-width: ` + this.MaxEmbedWidth + `;
			max-height: ` + this.MaxEmbedHeight + `;
		}

		.CEmbed_Image{
			max-width: ` + this.MaxEmbedWidth + `;
			max-height: ` + this.MaxEmbedHeight + `;

		}
	`;


  BdApi.injectCSS(this.getShortName(), this.style);
	this.YTEmbedHTML = `
		<div class="CEmbed_Pill">
		<iframe src="__LINK__"> </iframe>
		</div>
	`;

	this.MP4EmbedHTML = `
	<div class="CEmbed_Pill">
	<video class="CEmbed_Player" controls src=__LINK__> </video>
	<div>
	`;

	this.ImageEmbedHTML = `
	<div class="CEmbed_Pill">
	<a href="__LINK__">
	<img src="__LINK__" class="CEmbed_Image"></img>
	</a>
	</div>
	`;


	}

	unload() {}


	start() {
		var libraryScript = document.getElementById('zeresLibraryScript');
		if (libraryScript) libraryScript.parentElement.removeChild(libraryScript);
		libraryScript = document.createElement("script");
		libraryScript.setAttribute("type", "text/javascript");
		libraryScript.setAttribute("src", "https://rauenzi.github.io/BetterDiscordAddons/Plugins/PluginLibrary.js");
		libraryScript.setAttribute("id", "zeresLibraryScript");
		document.head.appendChild(libraryScript);

		if (typeof window.ZeresLibrary !== "undefined") this.initialize();
		else libraryScript.addEventListener("load", () => { this.initialize(); })
	}

	initialize() {
		this.initialized = true;
		//PluginUtilities.checkForUpdate(this.getName(), this.getVersion());

		this.ReEmbed();
		PluginUtilities.showToast(this.getName() + " " + this.getVersion() + " is ready to bring doom to your Embeds.");
	}

	stop() {
		BdApi.clearCSS(this.getShortName());
	}

	//change to improve images
	ReEmbed() {
		var messages = document.querySelectorAll('.message-first,.message ');

		for (var i = 0; i < messages.length; i++) {

			var embedRequestObjects;
			// Cleanup accessories
			var accessoryDoc = messages[i].querySelector('.accessory');


				embedRequestObjects = this.scanForLinks(accessoryDoc.innerHTML);

				for (var w = 0; w < accessoryDoc.children.length; w++) {

					if(this.isElementValidForDeletion(accessoryDoc.children[w])){
						accessoryDoc.removeChild(accessoryDoc.children[w]);
					}; // if clause

				}; // w loop

				if(messages[i].classList.contains('CEmbed_Parsed')){
					continue;
				}
				// add a class tag that marks this message as "done"
				messages[i].classList.add('CEmbed_Parsed')

				var markup = messages[i].querySelector('.markup');

				if(markup !== null){
					// Get Embed Request Objects for the current messages
					embedRequestObjects = embedRequestObjects.concat(this.scanForLinks(markup.innerHTML));


					embedRequestObjects = this.filterRequestObjectList(embedRequestObjects);

					var accessory = accessoryDoc;
					// Embed each object as a new Accessory
					for (var j = 0; j < embedRequestObjects.length; j++) {


						if(embedRequestObjects[j].Type == "YT"){
							// Do NOT Reembed youtube at the moment since it's a content type embed
							continue;
							var html = this.YTEmbedHTML;
							html = html.replace('__LINK__',embedRequestObjects[j].Url);

							var ndiv = document.createElement("div");
							ndiv.classList.add("CEmbed_Embedding")
							ndiv.innerHTML = html;
							accessory.appendChild(ndiv);
						}
						if(embedRequestObjects[j].Type == "MIME/MP4" || embedRequestObjects[j].Type == "MIME/WEBM" ){
							var html = this.MP4EmbedHTML;
							html = html.replace('__LINK__',embedRequestObjects[j].Url);
							var ndiv = document.createElement("div");
							ndiv.classList.add("CEmbed_Embedding")
							ndiv.innerHTML = html;
							accessory.appendChild(ndiv);
						}
						if(embedRequestObjects[j].Type == "IMG" ){
							var html = this.ImageEmbedHTML;
							var re = new RegExp('__LINK__', 'g');
							html = html.replace(re,embedRequestObjects[j].Url);
							var ndiv = document.createElement("div");
							ndiv.classList.add("CEmbed_Embedding")
							ndiv.innerHTML = html;
							accessory.appendChild(ndiv);
						}
					}
				}
		}; // i loop

		// Check each message for links
		var Embedded = false;

		// if something was embedded Scroll down
		if(Embedded){
			var elem = document.querySelector('.messages.scroller');
			setTimeout(()=>{
				if(elem != null){
					elem.scrollTop+=40000;
				}
			},300);
		}
	}


	observer(e){
		if (!e.addedNodes.length || !(e.addedNodes[0] instanceof Element) || !this.initialized) return;
		var elem = $(e.addedNodes[0]);

		if (elem.parents(".messages.scroller").length || elem.find(".message-group").parents(".messages.scroller").length) {
			this.ReEmbed();
		}

		if (elem.hasClass(".image").length || elem.find("span.image").parents(".messages.scroller").length) {
			this.ReEmbed();
		}
	}

	getSettingsPanel() {
		var panel = $("<form>").addClass("form").css("width", "100%");
		var header = $('<div class="formNotice-2tZsrh margin-bottom-20 padded card-3DrRmC">');
		var headerText = $('<div class="default-3bB32Y formText-1L-zZB formNoticeBody-1C0wup whiteText-32USMe modeDefault-389VjU primary-2giqSn">');


		headerText.html(this.SettingsHTML);



		headerText.css("line-height", "150%");
		headerText.appendTo(header);
		header.appendTo(panel);
		return panel[0];
	}



 // embed utility Functions
 // types : 'YT', 'MP4', 'WEBM', "IMAGE"
 makeEmbedRequestObject(type,url,position){
	 return {
		 Type : type,
		 Url : url,
		 Pos : position
	 };
 }


	// Traverses the DOM element tree recursively and checks if there is an element from the exception list in there, if not, return true
	isElementValidForDeletion(elem){
		return (this.CheckExceptionsRecursively(elem) == 0);
	}

	CheckExceptionsRecursively(elem){
		var exceptions = 0;

		for (var i = 0; i < this.deleteExceptions.length; i++) {
			if(elem.classList.contains(this.deleteExceptions[i])){
					exceptions++;
				}
		}
		// add exceptions of children
		for (var i = 0; i < elem.children.length; i++) {
			exceptions += this.CheckExceptionsRecursively(elem.children[i]);
		}
		return exceptions;
	}


	filterRequestObjectList(list){
			var UniqueLinks = [];

			for (var i = 0; i < list.length; i++) {

				if(UniqueLinks.includes(list[i].Url)){
					list.splice(i,1);
					i--;
				}else{

					// do not allow discord thumbnails and reloaded images
					var dcEXT_RE = /(cdn\.discordapp)|(discordapp\.net\/external\/)/gi;
					var r = list[i].Url.match(dcEXT_RE);
					if(r===null){

						UniqueLinks.push(list[i].Url);
					}
					else{

						list.splice(i,1);
						i--;
					}
				}
			}
			return list;
	}

	// scans a string for all link occurences and return a list of embedRequestObjects
	scanForLinks(message){
		var results = [];

		var linkRE = /http(?:s)?:\/\/(?:www\.)?\S+\.\S[^"\\<>]+/g;

		var str = JSON.stringify(message);

		// get matches in message with while loop, composing embedRequestObjects
		var match;
		while((match = linkRE.exec(str)) !== null ){
			results.push(this.makeEmbedRequestObject("UNKNOWN",match[0],match[0].index));
		}

		// filter duplicates due to html markup style
		var currentLink = ""
		for (var i = 0; i < results.length; i++) {
			if(results[i].Url != currentLink){
				currentLink = results[i].Url;
			}
			else{
				results.splice(i,1);
				i--;
			}
		}
		if(results.length == 0){
			return results;
		}

		// go over every every link and post-process embedRequestObjects
		for (var i = 0; i < results.length; i++) {
			results[i] = this.PostProcess_EmbedRequestObject(results[i]);
		}

		// return embedRequestObjects
		return results;

	}

	// receives a valid YT link without timestamps and converts it to a non shorthand embed link
	convertYTToEmbedLink(link, isShorthand){
		if(!isShorthand){
			var foo = link.split('/');
			var vidID = foo[foo.length-1];
			return 'https://www.youtube.com/embed/' + vidID;
		}
		else{
			return link.replace('watch?v=','embed/');
		}
	}



	// scans a message for Youtube links, returns a list of youtube links contained in that message and returns an embedObject
	PostProcess_EmbedRequestObject(_ERO){
		return this.PP_ERO_VID(
						 this.PP_ERO_IMAGE(
							this.PP_ERO_YT(_ERO)
							)
						);
	}

 // will take an embed request object and return the post processed object if it needed post processing, if not the original will be returned
 PP_ERO_YT(_ERO){
	 var fullRE = /https:\/\/(www\.)?youtube\.com\/watch\?v=([a-z0-9A-Z\-\_]*)/g;
	 var shRE = /https:\/\/(www\.)?youtu\.be\/([a-z0-9A-Z\-\_]*)/g;

 	 var r = _ERO.Url.match(fullRE);
 	 if(r!==null){
	  	_ERO.Url = this.convertYTToEmbedLink(r[0],false);
	  	_ERO.Type = "YT";
	  	return _ERO;
	 }

	 var r = _ERO.Url.match(shRE);
	 if(r!==null){
		 _ERO.Url = this.convertYTToEmbedLink(r[0],true);
		 _ERO.Type = "YT";
		 return _ERO;
	 }
	 return _ERO;
  }

	PP_ERO_IMAGE(_ERO){
		var imgRE = /http(s)?:\/\/(?:www\.)?.+(?:\.png|\.jpg|\.jpeg|\.gif)/gi
		var r = _ERO.Url.match(imgRE);
		if(r!==null){
			_ERO.Url = r[0];
			_ERO.Type = "IMG";
			return _ERO;
		}
		return _ERO;


	}
	PP_ERO_VID(_ERO){
		var RE = /http(?:s)?:\/\/(?:www\.)?.+(\.mp4|\.webm|\.gifv)/gi;

		var r =  RE.exec(_ERO.Url);// _ERO.Url.match(RE);

		if(r!==null){

			_ERO.Url = r[0];
			if(r[1] == ".mp4"){
				_ERO.Type = "MIME/MP4";
			}
			if(r[1] == ".webm"){
				_ERO.Type = "MIME/WEBM";
			}
			if(r[1] == ".gifv"){
				_ERO.Type = "MIME/MP4";
			}
			return _ERO;
		}
		return _ERO;

	}

}
